# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class TaxTemplate(ModelSQL, ModelView):
    _name = 'account.tax.template'

    payment_tax = fields.Boolean('Payment Tax',
            help='This tax is used for payments in Cash Basis '
                 'Accounting.')
    due_tax = fields.Many2One('account.tax.template',
        'Due Tax for Cash Basis Accounting', help='This tax '
        'will be used (as due tax) instead of the origin tax '
        'when doing payments in Cash Basis Accounting')

    def _get_tax_value(self, template, tax=None):
        res = super(TaxTemplate, self)._get_tax_value(template, tax=tax)
        if not tax or tax['payment_tax'] != template['payment_tax']:
            res['payment_tax'] = template['payment_tax']
        if not tax or tax['due_tax'] != template['due_tax']:
            res['due_tax'] = template['due_tax']
        return res

TaxTemplate()


class Tax(ModelSQL, ModelView):
    _name = 'account.tax'
    payment_tax = fields.Boolean('Payment Tax',
            help='This tax is used for payments in Cash Basis '
                 'Accounting.')
    due_tax = fields.Many2One('account.tax.template',
        'Due Tax for Cash Basis Accounting', help='This tax '
        'will be used (as due tax) instead of the origin tax '
        'when doing payments in Cash Basis Accounting')
Tax()

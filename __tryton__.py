# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Tax Cash Based',
    'name_de_DE': 'Buchhaltung Steuer Ist-Versteuerung',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Tax additions for cash based accounting
''',
    'description_de_DE': '''Steuererweiterungen für Ist-Versteuerung
''',
    'depends': [
        'account',
    ],
    'xml': [
        'account.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
